# Changelog

## 13.0.0. (2023-01-17)

### Breaking Changes

- This package is now pure ESM. Please [read this](https://gist.github.com/sindresorhus/a39789f98801d908bbc7ff3ecc99d99c).

## 12.1.0 (2022-08-05)

## 12.0.0 (2022-08-05)

## 11.0.0 (2022-08-05)

## 10.0.0 (2022-08-05)

## 9.1.2 (2022-08-05)

## 9.1.1 (2022-08-05)

## 9.1.0 (2022-08-05)

## 8.6.0 (2022-04-03)

## 8.5.6 (2022-03-02)

### Features

- sonarlint

### Bug Fixes

- add errpr
- bug
- debug output
- new lockfile
- remove bug
- sonar
- sonar
- sonarlint

## 8.5.5 (2021-12-18)

## 8.5.4 (2021-12-18)

## 8.5.3 (2021-12-17)

### Bug Fixes

- typo

## 8.5.2 (2021-12-17)

## 8.5.1 (2021-12-17)

## 8.5.0 (2021-12-16)

## 8.4.0 (2021-12-09)

## 8.3.0 (2021-12-07)

## 8.1.0 (2021-12-07)

### Bug Fixes

- **foo:** bar
- remove code smells

## 8.0.0 (2021-11-21)

## 7.0.0 (2021-11-10)

## 6.0.3 (2021-10-20)

## 6.0.2 (2021-10-05)

## 6.0.1 (2021-10-05)

### Features

- foobar

## 6.0.0 (2021-10-05)

### ⚠ BREAKING CHANGES

- This is a breaking change.

### Features

- add homepage
- add husky
- add standard-version
- export something
- foo
- foo
- foo
- foobar
- remove semantic-release
- semantic-release stuff
- something
- something
- something

### Bug Fixes

- add start script
- asdasdflk
- asdfjkl
- devDep
- **devDeps:** npm package is not required
- dkdkdks
- **docs:** foobar
- **docs:** foobar2
- foobar
- foobar
- log msg
- mark as executive
- packages
- pipeline
- pipeline
- pipeline
- pipeline
- pipeline
- some test
- something
- something
- something
- something
- something
- something
- something
- something
- test repo url
- test repo url
- test repo url
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update package.json
- url in package.json
- versions

## 5.0.1 (2021-10-03)

### Features

- export something

## 5.0.0 (2021-10-03)

## 4.0.14 (2021-09-20)

### Miscellaneous Chores

- pin dependencies
- update dependency @4s1/changelog-config to v2

### Documentation

- fix changelog
- fix changelog
- foo

## 4.0.13 (2021-09-17)

### Miscellaneous Chores

- foo

### Continuous Integration

- fix pipeline

## 4.0.12 (2021-09-17)

### Miscellaneous Chores

- foo

## 4.0.11 (2021-09-17)

## 4.0.10 (2021-09-17)

## 4.0.9 (2021-09-17)

### Miscellaneous Chores

- foo
- foo

## 4.0.8 (2021-09-17)

### Documentation

- remove header in changelog
