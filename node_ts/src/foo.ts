// eslint-disable-next-line @typescript-eslint/no-inferrable-types
const someText: string = "Hello World";
console.log(someText);

// --------------------------------------------

enum Bar {
  A,
  B,
  C,
}

// --------------------------------------------

class Dummy {
  constructor(public name: string) {}
}
